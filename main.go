package main

import (
	"bufio"
	"encoding/csv"
	"flag"
	"fmt"
	"io"
	"os"
	"strings"
	"testing"
	"time"
)

var filename string
var timeLimit int

func init() {
	testing.Init()

	flag.StringVar(&filename, "filename", "problems.csv", "The file to load for the quiz")
	flag.IntVar(&timeLimit, "time", 30, "The time limit for the quiz in seconds")
	flag.Parse()
}

func main() {
	//read in problems
	lines := readCSVFile(filename)
	problems := parseLines(lines)

	//create timer
	timer := time.NewTimer(time.Duration(timeLimit) * time.Second)

	//run quiz
	correct := 0
	for i, p := range problems {
		// ask question
		answerCh := make(chan string)
		go askQuestion(i+1, p.q, os.Stdin, answerCh)

		select {
		case <-timer.C: //times up
			fmt.Printf("\nYou scored %d out of %d correct.\n", correct, len(problems))
			return
		case answer := <-answerCh: //got answer
			if answer == p.a {
				correct++
			}
		}
	}

	fmt.Printf("You scored %d out of %d correct.\n", correct, len(problems))
}

func askQuestion(number int, question string, stdin io.Reader, answerCh chan<- string) {
	fmt.Printf("Problem #%d: %s = ", number, question)

	var answer string
	reader := bufio.NewReader(stdin)
	answer, err := reader.ReadString('\n')
	answer = strings.TrimSuffix(answer, "\n")
	answer = strings.TrimSuffix(answer, "\r")

	if err != nil {
		fmt.Println("There was an error!")
		panic(err)
	}

	answerCh <- answer
}

type problem struct {
	q string
	a string
}

func parseLines(lines [][]string) []problem {
	problems := make([]problem, len(lines))

	for i, line := range lines {
		problems[i] = problem{
			q: line[0],
			a: line[1],
		}
	}

	return problems
}
func readCSVFile(filename string) [][]string {
	file, err := os.Open(filename)
	if err != nil {
		exit(fmt.Sprintf("Failed to open the CSV file: %s\n", filename))
	}

	reader := csv.NewReader(file)
	lines, err := reader.ReadAll()
	if err != nil {
		exit("Failed to parse the provided CSV file.")
	}

	return lines
}

func Calculate(n int) int {
	return n + 2
}

func exit(msg string) {
	fmt.Println(msg)
	os.Exit(1)
}
