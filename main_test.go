package main

import (
	"bytes"
	"fmt"
	"testing"
)

func TestCalculate(t *testing.T) {
	if Calculate(2) != 4 {
		t.Error("Expected 2 + 2 to equal 4")
	}
}

func TestAskingQuestionsWindows(t *testing.T) {
	var stdin bytes.Buffer
	stdin.Write([]byte("answer\r\n"))

	ansCh := make(chan string)
	go askQuestion(1, "question", &stdin, ansCh)
	answer := <-ansCh

	assertStringEqual(t, "answer", answer)
}

func TestAskingQuestionsLinux(t *testing.T) {
	var stdin bytes.Buffer
	stdin.Write([]byte("answer\n"))

	ansCh := make(chan string)
	go askQuestion(1, "question", &stdin, ansCh)
	answer := <-ansCh

	assertStringEqual(t, "answer", answer)
}

func TestTableCalculate(t *testing.T) {
	var tests = []struct {
		input    int
		expected int
	}{
		{2, 4},
		{-1, 1},
		{0, 2},
		{99999, 100001},
	}

	for _, test := range tests {
		if output := Calculate(test.input); output != test.expected {
			t.Error("Test Failed: {} inputted, {} expected, recieved: {}", test.input, test.expected, output)
		}
	}
}

func assertStringEqual(t *testing.T, expected string, actual string) {
	if expected != actual {
		t.Error(fmt.Sprintf("Expected %s, but was %s", expected, actual))
	}
}
